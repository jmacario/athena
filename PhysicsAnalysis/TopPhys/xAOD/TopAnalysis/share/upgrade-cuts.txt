LibraryNames libTopEventSelectionTools libTopEventReconstructionTools

#These aren't used in upgrade studies, just filled so top-xaod doesn't complain
GRLDir  GoodRunsLists
GRLFile data15_13TeV/20160720/physics_25ns_20.7.xml data16_13TeV/20161101/physics_25ns_20.7.xml
PRWConfigFiles
PRWLumiCalcFiles GoodRunsLists/data15_13TeV/20160720/physics_25ns_20.7.lumicalc.OflLumi-13TeV-005.root GoodRunsLists/data16_13TeV/20161101/physics_25ns_20.7.lumicalc.OflLumi-13TeV-005.root

TDPPath TopUpgradeGrid/XSection-MC15-14TeV.data
MCGeneratorWeights True


ElectronCollectionName None
MuonCollectionName None
JetCollectionName None
LargeJetCollectionName None
LargeJetSubstructure None
TauCollectionName None
PhotonCollectionName None

TruthCollectionName TruthParticles
TruthJetCollectionName AntiKt4TruthDressedWZJets
TruthLargeRJetCollectionName AntiKt10TruthTrimmedPtFrac5SmallR20Jets
TopPartonHistory ttbar
TopParticleLevel True
TruthBlockInfo False
TruthPhotonCollectionName TruthPhotons
PDFInfo True

ObjectSelectionName top::ObjectLoaderStandardCuts
OutputFormat top::EventSaverFlatNtuple
OutputEvents SelectedEvents
OutputFilename output.root
PerfStats No

Systematics All
JetUncertainties_NPModel GlobalReduction

ElectronID MediumLH
ElectronIDLoose LooseAndBLayerLH
ElectronIsolation Gradient
ElectronIsolationLoose None
ElectronPt 7000.

MuonQuality Medium
MuonQualityLoose Medium
MuonIsolation Gradient
MuonIsolationLoose None
MuonPt 4000. #j/psi
TruthMuonPt 4000. #j/psi

JetPt 25000.
JetEta 2.5

TruthPhotonEta 2.37         #default is 2.5
TruthPhotonPt 15000.        #default is 25000.
#PhotonID None          #default is Tight
#PhotonIDLoose None     #default is Loose
#PhotonIsolation None   #default is FixedCutTight
#PhotonIsolationLoose None #default is FixedCutLoose


# DoTight/DoLoose to activate the loose and tight trees
# each should be one in: Data, MC, Both, False
DoTight Both
DoLoose Data

UseAodMetaData False
IsAFII False

BTaggingCaloJetWP FixedCutBEff_77

HLLHC True
HLLHCFakes True


########################
### Muon+jets selection
########################
SELECTION mujets_upgrade
INITIAL
MU_N 25000 >= 1
EL_N 25000 == 0
MU_N 25000 == 1
JET_N 25000 >= 1
JET_N 25000 >= 2
SAVE

########################
### Electron+jets selection
########################
SELECTION ejets_upgrade
INITIAL
EL_N 25000 >= 1
MU_N 25000 == 0
EL_N 25000 == 1
JET_N 25000 >= 1
JET_N 25000 >= 2
SAVE


########################
### Muon+1jet selection
########################
SELECTION mu1jet_upgrade
INITIAL
MU_N 25000 >= 1
EL_N 25000 == 0
MU_N 25000 == 1
JET_N 25000 >= 1
SAVE

########################
### Electron+1jet selection
########################
SELECTION e1jet_upgrade
INITIAL
EL_N 25000 >= 1
MU_N 25000 == 0
EL_N 25000 == 1
JET_N 25000 >= 1
SAVE


### ttV
#TRILEPTON

###
### dead cone:
###
SELECTION deadcone
INITIAL
EL_N 25000 == 1
MU_N 25000 == 0
JET_N 25000 >= 1
JET_N 25000 >= 2
MET > 25000
#JET_N_BTAG FixedCutBEff_85 >= 1
LJET_N 200000 >= 2
LJET_N 300000 >= 1
#LJET_TOPTAGGED tagged_80 == 1
#LJET_CONTAINS_LEPTON 200000 == 1
#LJET_CONTAINS_B_AND_LEPTON FixedCutBEff_85 >= 1
#LEPTONIC_B_LJET_DECOMPOSED ungroomed == 1
SAVE

SELECTION mujets_trimming_basic
INITIAL
MU_N 25000 == 1
EL_N 25000 == 0
TRIGMATCH
JET_N 25000 >= 1
JET_N 25000 >= 2
MET > 25000
#JET_N_BTAG FixedCutBEff_85 >= 1
LJET_N 200000 >= 2
LJET_N 300000 >= 1
#LJET_TOPTAGGED tagged_80 == 1
#LJET_CONTAINS_LEPTON 200000 == 1
#LJET_CONTAINS_B_AND_LEPTON FixedCutBEff_85 >= 1
#LEPTONIC_B_LJET_DECOMPOSED trimming == 1 
SAVE

### ======== ttgamma:

SELECTION ejets_gamma_basic
INITIAL
EL_N 25000 == 1
MU_N 25000 == 0
TRIGMATCH
JETCLEAN LooseBad
NOBADMUON
PH_N 10000 >= 1
SAVE

SELECTION mujets_gamma_basic
INITIAL
MU_N 25000 == 1
EL_N 25000 == 0
TRIGMATCH
JETCLEAN LooseBad
NOBADMUON
PH_N 10000 >= 1
SAVE

SELECTION emu_gamma_basic
INITIAL
EL_N 25000 >= 1
MU_N 25000 >= 1
TRIGMATCH
JETCLEAN LooseBad
EL_N 25000 == 1
MU_N 25000 == 1
OS
#MLL > 15000
NOBADMUON
PH_N 10000 >= 1
SAVE

SELECTION ee_gamma_basic
INITIAL
EL_N 25000 >= 2
TRIGMATCH
JETCLEAN LooseBad
EL_N 25000 == 2
MU_N 25000 == 0
OS
#MLL > 15000
NOBADMUON
PH_N 10000 >= 1
SAVE

SELECTION mumu_gamma_basic
INITIAL
MU_N 25000 >= 2
TRIGMATCH
JETCLEAN LooseBad
MU_N 25000 == 2
EL_N 25000 == 0
OS
#MLL > 15000
NOBADMUON
PH_N 10000 >= 1
SAVE


###======== tZ:

SELECTION mumumu
INITIAL
MU_N 15000 >= 3
MU_N 15000 == 3
EL_N 15000 == 0
JET_N 30000 >= 1
JET_N 30000 >= 2
SAVE

SELECTION eee
INITIAL
EL_N 15000 >= 3
EL_N 15000 == 3
MU_N 15000 == 0
JET_N 30000 >= 1
JET_N 30000 >= 2
SAVE

SELECTION eemu
INITIAL
MU_N 15000 >= 1
EL_N 15000 >= 1
EL_N 15000 >= 2
MU_N 15000 == 1
EL_N 15000 == 2
JET_N 30000 >= 1
JET_N 30000 >= 2
SAVE

SELECTION emumu
INITIAL
EL_N 15000 >= 1
MU_N 15000 >= 1
MU_N 15000 >= 2
EL_N 15000 == 1
MU_N 15000 == 2
JET_N 30000 >= 1
JET_N 30000 >= 2
SAVE

###======== Jpsi top mass:
### l+>=4 jets && all other muons (i need to go as low as 4 GeV tight muons).

SELECTION ejets_jpsi_basic
INITIAL
EL_N 25000 == 1
MU_N 25000 == 0
TRIGMATCH
JETCLEAN LooseBad
JET_N 25000 >= 1
JET_N 25000 >= 2
JET_N 25000 >= 3
JET_N 25000 >= 4
SAVE

SELECTION mujets_jpsi_basic
INITIAL
MU_N 25000 >= 1
EL_N 25000 == 0
TRIGMATCH
JETCLEAN LooseBad
JET_N 25000 >= 1
JET_N 25000 >= 2
JET_N 25000 >= 3
JET_N 25000 >= 4
SAVE


###======== t->Zq FCNC:
SELECTION sel1_tZq
INITIAL
EL_N 15000 == 1
MU_N 15000 == 2
JET_N 25000 >= 1
SAVE

SELECTION sel2_tZq
INITIAL
MU_N 15000 == 1
EL_N 15000 == 2
JET_N 25000 >= 1
SAVE

SELECTION sel3_tZq
INITIAL
EL_N 15000 == 3
MU_N 15000 == 0
JET_N 25000 >= 1
SAVE

SELECTION sel4_tZq
INITIAL
MU_N 15000 == 3
EL_N 15000 == 0
JET_N 25000 >= 1
SAVE

########################
### Di-muon selection
########################
SELECTION mumu
INITIAL
MU_N 25000 >= 2
MU_N 25000 == 2
EL_N 25000 == 0
#MLL > 15000
JET_N 25000 >= 1
JET_N 25000 >= 2
SAVE

########################
### Di-electron selection
########################
SELECTION ee
INITIAL
EL_N 25000 >= 2
EL_N 25000 == 2
MU_N 25000 == 0
#MLL > 15000
JET_N 25000 >= 1
JET_N 25000 >= 2
SAVE

########################
### e-mu selection
########################
SELECTION emu
INITIAL
MU_N 25000 >= 1
EL_N 25000 >= 1
MU_N 25000 == 1
EL_N 25000 == 1
#MLL > 15000
JET_N 25000 >= 1
JET_N 25000 >= 2
SAVE
